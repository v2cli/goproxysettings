# Go Proxy Settings
A painless cross-platform(Windows/MacOS/Linux) library for setting up system proxy.

~~~shell
go get gitlab.com/v2cli/goproxysettings
~~~

## Features
 - Working through common used platforms: **Linux(GNOME, KDE), Windows(HTTP Proxy Only), MacOSX**
 - Setting up system http/socks5 proxy without pain

## Usage
~~~go
// create proxy instance
p := goproxysettings.Proxy{
    IsSocks: true,
    Address: "127.0.0.1",
    Port: 10808,
}
// setup proxy settings with proxy object
goproxysettings.SetupProxy(p)
// clear proxy settings
goproxysettings.ClearProxy()
~~~