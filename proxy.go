package goproxysettings

import (
	"fmt"
	"strconv"
)

type Proxy struct {
	IsSocks bool
	Address string
	Port    uint16
}

func (p Proxy) Addr() string {
	return fmt.Sprintf("%s:%d", p.Address, p.Port)
}

func (p Proxy) PortStr() string {
	return strconv.Itoa(int(p.Port))
}
