//go:build openbsd || freebsd
// +build openbsd freebsd

package goproxysettings

import "errors"

func SetupProxy(proxy Proxy) error {
	return errors.New("os not supported: " + runtime.GOOS)
}

func ClearProxy(proxy Proxy) error {
	return errors.New("os not supported: " + runtime.GOOS)
}
