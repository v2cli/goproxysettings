//go:build linux
// +build linux

package goproxysettings

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"strings"
)

func SetupProxy(proxy Proxy) error {
	session := strings.ToLower(strings.TrimSpace(os.Getenv("XDG_SESSION_DESKTOP")))
	if session == "kde" || session == "plasma" {
		exec.Command("kwriteconfig5", "--file", "~/.config/kioslaverc", "--group", "\"Proxy Settings\"", "--key", "\"ProxyType\"", "1").Run()
		if proxy.IsSocks {
			addr := fmt.Sprintf("socks://%s:%d", proxy.Address, proxy.Port)
			exec.Command("kwriteconfig5", "--file", "~/.config/kioslaverc", "--group", "\"Proxy Settings\"", "--key", "\"socksProxy\"", addr).Run()
		} else {
			addr := fmt.Sprintf("http://%s:%d", proxy.Address, proxy.Port)
			exec.Command("kwriteconfig5", "--file", "~/.config/kioslaverc", "--group", "\"Proxy Settings\"", "--key", "\"httpProxy\"", addr).Run()
			exec.Command("kwriteconfig5", "--file", "~/.config/kioslaverc", "--group", "\"Proxy Settings\"", "--key", "\"httpsProxy\"", addr).Run()
		}
		exec.Command("dbus-send", "--type=signal", "/KIO/Scheduler", "org.kde.KIO.Scheduler.reparseSlaveConfiguration", "string:''").Run()
		return nil
	} else if session == "gnome" {
		exec.Command("gsettings", "set", "org.gnome.system.proxy", "mode", "\"manual\"").Run()
		if proxy.IsSocks {
			exec.Command("gsettings", "set", "org.gnome.system.proxy.socks", "host", proxy.Address).Run()
			exec.Command("gsettings", "set", "org.gnome.system.proxy.socks", "port", proxy.PortStr()).Run()
		} else {
			exec.Command("gsettings", "set", "org.gnome.system.proxy.http", "host", proxy.Address).Run()
			exec.Command("gsettings", "set", "org.gnome.system.proxy.http", "port", proxy.PortStr()).Run()
			exec.Command("gsettings", "set", "org.gnome.system.proxy.https", "host", proxy.Address).Run()
			exec.Command("gsettings", "set", "org.gnome.system.proxy.https", "port", proxy.PortStr()).Run()
		}
		return nil
	} else {
		return errors.New("your de/command line not supported")
	}
}

func ClearProxy(proxy Proxy) error {
	session := strings.ToLower(strings.TrimSpace(os.Getenv("XDG_SESSION_DESKTOP")))
	if session == "kde" || session == "plasma" {
		return exec.Command("kwriteconfig5", "--file", "~/.config/kioslaverc", "--group", "\"Proxy Settings\"", "--key", "\"ProxyType\"", "0").Run()
	} else if session == "gnome" {
		return exec.Command("gsettings", "set", "org.gnome.system.proxy", "mode", "\"none\"").Run()
	} else {
		return errors.New("your de/command line not supported")
	}
}
