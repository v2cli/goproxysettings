//go:build darwin
// +build darwin

package goproxysettings

import (
	"os/exec"
	"strings"
)

const (
	NETWORK_SETUP = "/usr/sbin/networksetup"
	NETWORK_ON    = "on"
	NETWORK_OFF   = "off"
)

func SetupProxy(proxy Proxy) error {
	services, err := listNetworkServices()
	if err != nil {
		return err
	}
	for _, s := range services {
		exec.Command(NETWORK_SETUP, "-setautoproxystate", s, NETWORK_OFF).Run()
		if proxy.IsSocks {
			changeHttpProxyStateFor(s, NETWORK_OFF)
			changeSocksProxyStateFor(s, NETWORK_ON)
			exec.Command(NETWORK_SETUP, "-setsocksfirewallproxystate", s, proxy.Address, proxy.PortStr()).Run()
		} else {
			changeHttpProxyStateFor(s, NETWORK_ON)
			changeSocksProxyStateFor(s, NETWORK_OFF)
			exec.Command(NETWORK_SETUP, "-setwebproxystate", s, proxy.Address, proxy.PortStr()).Run()
			exec.Command(NETWORK_SETUP, "-setsecurewebproxystate", s, proxy.Address, proxy.PortStr()).Run()
		}
	}
	return nil
}

func ClearProxy() error {
	services, err := listNetworkServices()
	if err != nil {
		return err
	}
	for _, s := range services {
		exec.Command(NETWORK_SETUP, "-setautoproxystate", s, NETWORK_OFF).Run()
		changeHttpProxyStateFor(s, NETWORK_OFF)
		changeSocksProxyStateFor(s, NETWORK_OFF)
	}
	return nil
}

func listNetworkServices() ([]string, error) {
	outBytes, err := exec.Command(NETWORK_SETUP, "-listallnetworkservices").Output()
	if err != nil {
		return nil, err
	}
	services := make([]string, 0)
	for i, l := range strings.Split(string(outBytes), "\n") {
		l = strings.TrimSpace(l)
		if i == 0 || strings.Contains(l, "*") {
			continue
		}
		services = append(services, l)
	}
	return services, nil
}

func changeHttpProxyStateFor(s, state string) {
	exec.Command(NETWORK_SETUP, "-setsocksfirewallproxystate", s, state).Run()
}

func changeSocksProxyStateFor(s, state string) {
	exec.Command(NETWORK_SETUP, "-setwebproxystate", s, state).Run()
	exec.Command(NETWORK_SETUP, "-setsecurewebproxystate", s, state).Run()
}
